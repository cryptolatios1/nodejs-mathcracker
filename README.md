# nodejs-mathcracker

Have you ever wanted to know which arithmetic operations can lead to the desired result?
Mathcracker allows you to bruteforce various operations on a random basis

```bash
$node mathcracker.mjs 3 2 9
> [ '(3**2)', '(3*3)' ]

$node mathcracker.mjs 3 2 10
> [ '((2**3)+2)', '((2+3)*2)', '((3+2)*2)' ]
```
