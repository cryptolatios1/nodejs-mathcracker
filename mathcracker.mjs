import crypto from "node:crypto";

let args = process.argv.slice(2);
if(args.length < 2) {
	process.stdout.write("usage: [input1] <...inputN> [output]\n");
}

/* number to receive */
let output = Number(args.pop());

/* input-numbers (it's possible that not all numbers are used) */
let input = args.map(a => Number(a));

/* set the limit how many times the program tries to get the result */
/* todo set in config */
let maxrounds = 100000;

/* max operations per iteration */
/* todo set in config */
let maxops = 2;

/* all possible operations: addition, subtraction, division, multiplication, exponent, root */
let events = ["+", "-", "/", "*", "**", "r"];

/* all the formulars that equals to the output */
let results = [];

for(let index = 0; index < maxrounds; index++) {
	let tempResult = input[crypto.randomInt(input.length)];
	let tempOps = [tempResult];
	for(let index2 = 0; index2 < maxops; index2++) {
		let randomEvent = events[crypto.randomInt(events.length)];
		let randomKey = input[crypto.randomInt(input.length)];
		switch(randomEvent) {
			case "+": {
				tempResult = tempResult + randomKey;
				tempOps.push("+");
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};

			case "-": {
				tempResult = tempResult - randomKey;
				tempOps.push("-");
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};

			case "/": {
				tempResult = tempResult / randomKey;
				tempOps.push("/");
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};

			case "*": {
				tempResult = tempResult * randomKey;
				tempOps.push("*");
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};

			case "**": {
				tempResult = tempResult**randomKey;
				tempOps.push("**");
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};

			case "r": {
				tempResult = tempResult**(1/randomKey);
				tempOps.push("root"); /* todo unpush <randomKey>√(<x>) */
				tempOps.push(randomKey);
				tempOps.push(")");
				break;
			};
		};

		if(tempResult === output) {
			for(let index3 = 0; index3 <= index2; index3++) {
				tempOps.unshift("(");
			};
			index2 = maxops;

			let tempOpString = tempOps.join("");
			if(!results.includes(tempOpString)) {
				results.push(tempOpString);
			};
		};
	};
};

console.log(results);